package com.getengineeringtroops.ioemocktest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {

    int score;
    TextView total;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        total=(TextView) findViewById(R.id.textViewTotalCorrect);
        score=getIntent().getIntExtra("result",0);
        total.setText(score+" ");
    }

    public void viewSolutionClicked(View view){
        Intent intent= new Intent(this,SolutionActivity.class);
        startActivity(intent);
    }
}
