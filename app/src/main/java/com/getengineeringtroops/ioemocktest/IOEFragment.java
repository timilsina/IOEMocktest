package com.getengineeringtroops.ioemocktest;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by phantom on 2/3/16.
 */
public class IOEFragment extends Fragment implements View.OnClickListener{

    ImageView imageViewTest;
    ImageView imageViewResult;
    String url="http://onlinemocktest.herokuapp.com/api/dashboard";
    SharedPreferences sharedPreferences;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences=getActivity().getSharedPreferences("",Context.MODE_PRIVATE);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_dashboard,container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        imageViewTest=(ImageView) getActivity().findViewById(R.id.imageViewTest);
        imageViewResult=(ImageView) getActivity().findViewById(R.id.imageViewResult);
        imageViewTest.setOnClickListener(this);
        imageViewResult.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle("IOE Mocktest");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imageViewTest:
            {
                Intent intent=new Intent(getActivity(),QuestionListActivity.class);
                getActivity().startActivity(intent);
                break;
            }
            case R.id.imageViewResult:
            {
                Intent intent=new Intent(getActivity(),TestActivity.class);
                getActivity().startActivity(intent);
                break;
            }
        }
    }

    public void getQuestion(){
        JsonObjectRequest jsonObjectRequest =new JsonObjectRequest(Request.Method.GET, url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                parseJson(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(),"Error in Network Connection",Toast.LENGTH_SHORT).show();
            }
        }){
           @Override
        public Map<String,String> getHeaders(){
               HashMap<String,String> headers=new HashMap<>();
               headers.put("username",sharedPreferences.getString("username",null));
               headers.put("password",sharedPreferences.getString("password",null));
               return headers;
           }
        };
    }

    public void parseJson(JSONObject jsonObject){
        Intent intent=new Intent(getActivity(),QuestionListActivity.class);
        intent.putExtra("questionList",jsonObject.toString());
        startActivity(intent);
    }
}
