package com.getengineeringtroops.ioemocktest;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by phantom on 2/11/16.
 */
public class QuestionListAdapter extends BaseAdapter {

    Context context;
    List<String> question;
    List<Boolean> locked;
    public QuestionListAdapter(Context _context,List<String> _question,List<Boolean> _locked) {
        super();
        this.context=_context;
        this.question=_question;
        this.locked=_locked;
    }

    @Override
    public int getCount() {
        return question.size();
    }

    @Override
    public Object getItem(int position) {
        return this.question.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String ques=question.get(position);
        Boolean bool=locked.get(position);
        if(convertView==null){
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_questions, null);
        }
        TextView textView=(TextView) convertView.findViewById(R.id.textViewQuestionsList);
        ImageView imageView=(ImageView) convertView.findViewById(R.id.imageViewLock);
        textView.setText(ques);
        if(bool==Boolean.FALSE){
            imageView.setImageResource(R.drawable.locked);
        }
        else imageView.setImageResource(R.drawable.unlocked);
        return convertView;
    }
}
