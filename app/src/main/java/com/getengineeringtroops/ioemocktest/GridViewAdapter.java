package com.getengineeringtroops.ioemocktest;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


/**
 * Created by phantom on 2/13/16.
 */
public class GridViewAdapter extends BaseAdapter{

    private Context context;
    private int length;
    public GridViewAdapter(Context _context,int _length) {
        super();
        this.context=_context;
        this.length=_length;
    }

    @Override
    public int getCount() {
        return length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return super.hasStableIds();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            LayoutInflater layoutInflater=(LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.grid_view_items,null);
        }
        TextView textView=(TextView) convertView.findViewById(R.id.textViewGridView);
        textView.setText((position+1)+"");
        return convertView;
    }
}
