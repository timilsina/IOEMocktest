package com.getengineeringtroops.ioemocktest;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by phantom on 2/5/16.
 */
public class IOMFragment extends Fragment{

    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState){
        return inflater.inflate(R.layout.fragment_dashboard,container,false);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle("IOM Mocktest");
    }
}
