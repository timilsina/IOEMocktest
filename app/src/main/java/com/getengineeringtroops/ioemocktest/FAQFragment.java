package com.getengineeringtroops.ioemocktest;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by phantom on 2/7/16.
 */
public class FAQFragment extends Fragment{

    MyExpandableAdapter expandableAdapter;
    ExpandableListView expandableListView;
    List<String> parent;
    HashMap<String,List<String>> children;

    String defaultValue="<!DOCTYPE html>\n" +
            "<html>\n" +
            "<head>\n" +
            "\t<title>mathjax</title>\t\n" +
            "\t<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n" +
            "\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />\n" +
            "\t  <script type=\"text/x-mathjax-config\">\n" +
            "\t  MathJax.Hub.Config({\n" +
            "\t    tex2jax: {inlineMath: [[\"$\",\"$\"],[\"\\\\(\",\"\\\\)\"]]}\n" +
            "\t  });\n" +
            "\t</script>\n" +
            "\n" +
            "\t<script type=\"text/javascript\" src=\"MathJax.js?config=TeX-AMS_HTML-full\"></script>\n" +
            "</head>\n" +
            "<body>\n" +
            "<p>\n" +
            "When $a \\ne 0$, there are two solutions to \\(ax^2 + bx + c = 0\\) and they are\n" +
            "$$x = {-b \\pm \\sqrt{b^2-4ac} \\over 2a}.$$\n" +
            "</p>\n" +
            "</body>\n" +
            "</html>";
    String temp="";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        parent= new ArrayList<>();
        children =new HashMap<String,List<String>>();
        parent.add("How do I recharge my account?");
        parent.add("How can I find recharge outlets");
        parent.add("The answer for x question is wrong how can I report?");
        parent.add("Lorem ipsum ");
        List<String> child1 = new ArrayList<String>();
        child1.add("blah blah blah");
        children.put(parent.get(0), child1);
        List<String> child2 = new ArrayList<String>();
        child2.add("blah blah blah");
        children.put(parent.get(1),child2);
        List<String> child3 = new ArrayList<String>();
        child3.add("blah blah blah");
        children.put(parent.get(2),child3);
        List<String> child4 = new ArrayList<String>();
        child4.add("blah blah blah");
        children.put(parent.get(3),child4);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_faq,container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        //Loading expandable listView

        expandableListView =(ExpandableListView) getActivity().findViewById(R.id.expandableListView);
        expandableAdapter=new MyExpandableAdapter(getActivity(), parent,children);
        expandableListView.setAdapter(expandableAdapter);

    }

    @Override
    public void onResume() {
        super.onResume();

        //checking MathJax
//        WebView webView=(WebView) getActivity().findViewById(R.id.webView);
//        webView.getSettings().setLoadWithOverviewMode(true);
//        webView.getSettings().setUseWideViewPort(true);
//        webView.getSettings().setJavaScriptEnabled(true);
//        webView.loadDataWithBaseURL("file:///android_asset/MathJax/",defaultValue,"text/html","UTF-8",null);
//        webView.loadUrl("https://www.tuhh.de/MathJax/test/sample-tex.html");

    }
}