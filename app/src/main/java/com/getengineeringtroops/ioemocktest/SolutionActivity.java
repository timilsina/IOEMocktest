package com.getengineeringtroops.ioemocktest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by phantom on 2/12/16.
 */
public class SolutionActivity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_solution);
    }
}
