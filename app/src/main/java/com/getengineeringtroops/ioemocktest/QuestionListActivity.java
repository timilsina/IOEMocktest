package com.getengineeringtroops.ioemocktest;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QuestionListActivity extends AppCompatActivity {

    public static final String IOE_TAG="ioe_set";
    String url="http://onlinemocktest.herokuapp.com/api/questions/ioe/150";
    ProgressBar loading;
    String[] questionList={"Question Set 1","Question Set 2","Question Set 3","Question Set 4","Question Set 5","Question Set 6","Question Set 7"};
    Boolean[] booleans={true,true,true,true,false,false,false,false};
    QuestionListAdapter questionListAdapter;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_list);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        sharedPreferences=getSharedPreferences("com.getengineeringtroops.ioemocktest", Context.MODE_PRIVATE);

        String json=getIntent().getStringExtra("json");
        try {
            JSONObject object=new JSONObject(json);
            parseJson(object);
        }
        catch (Throwable throwable){
            Log.d("questionActivity","Error parsing Json");
        }

        loading =(ProgressBar) findViewById(R.id.progressBar);
        ListView listView=(ListView) findViewById(R.id.listView);
        List<String> ques= Arrays.asList(questionList);
        List<Boolean> bool=Arrays.asList(booleans);
        questionListAdapter=new QuestionListAdapter(this,ques,bool);
        listView.setAdapter(questionListAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                loading.setVisibility(View.VISIBLE);
                sendRequest();
            }
        });
    }

    public void sendRequest(){
        RequestQueue requestQueue= Volley.newRequestQueue(this);

        JsonArrayRequest jsonArrayRequest=new JsonArrayRequest(Method.GET, url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.d("haha", response.toString());
                startMocktest(response);
                loading.setVisibility(View.GONE);
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(QuestionListActivity.this,"Error in Network Connection",Toast.LENGTH_SHORT).show();
                loading.setVisibility(View.GONE);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String>  headers = new HashMap<String, String>();
                headers.put("username", sharedPreferences.getString("username",null));
                headers.put("password", sharedPreferences.getString("password",null));

                return headers;
            }
        };
        requestQueue.add(jsonArrayRequest);
    }

    public void startMocktest(JSONArray response){
        Intent intent=new Intent(getApplicationContext(),TestActivity.class);
        intent.putExtra("json",response.toString());
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
    }

    public void parseJson(JSONObject jsonObject){
        String[] questionset;
        String[] score;
        try {
            JSONArray array=jsonObject.getJSONArray(IOE_TAG);
            Integer length=array.length();
            questionset=new String[length];
            score=new String[length];
            for(int i=0;i<length;i++){
                jsonObject=array.getJSONObject(i);
                questionset[i]=jsonObject.getString("questionset");
                score[i]=jsonObject.getString("score");
            }
        }
        catch (JSONException e){
            e.printStackTrace();
        }
    }

    public void setListView(){

    }
}
