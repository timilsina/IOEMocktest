package com.getengineeringtroops.ioemocktest;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity{

    SharedPreferences sharedPreferences;
    ProgressDialog progressDialog;
    String username;
    String password;
    String url="http://onlinemocktest.herokuapp.com/api/login";
    EditText usernameEditText;
    EditText passwordEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        sharedPreferences=getSharedPreferences("com.getengineeringtroops.ioemocktest", Context.MODE_PRIVATE);
        usernameEditText=(EditText) findViewById(R.id.editTextUsername);
        passwordEditText=(EditText) findViewById(R.id.editTextPassword);
    }

    @Override
    public void onStart(){
        super.onStart();
        username=sharedPreferences.getString("username",null);
        password=sharedPreferences.getString("password", null);
        usernameEditText.setText(username);
        passwordEditText.setText(password);
        if(!(username==null)){
            login();
        }
    }

    public void LoginClicked(View view){
        login();
    }

    public void signUpClicked(View view){
        Intent intent =new Intent(this,FormActivity.class);
        startActivity(intent);
    }

    public void forgotPasswordClicked(View view){
        Intent intent = new Intent(this,ForgotPasswordActivity.class);
        startActivity(intent);
    }

    public void login(){
        progressDialog=new ProgressDialog(this);
        progressDialog.setTitle("Mocktest");
        progressDialog.setMessage("Logging In.....");
        progressDialog.show();

        StringRequest stringRequest=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response.equals("226")){
                    SharedPreferences.Editor editor=sharedPreferences.edit();
                    editor.putString("username",usernameEditText.getText().toString());
                    editor.putString("password",passwordEditText.getText().toString());
                    editor.commit();
                    progressDialog.dismiss();
                    Intent intent =new Intent(LoginActivity.this,MainActivity.class);
                    startActivity(intent);
                }
                else {
                    Toast.makeText(getApplication(),"Username or Password wrong",Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(),"Error in Network Connection",Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            public Map<String,String> getParams() throws AuthFailureError{
                HashMap<String,String> headers=new HashMap<>();
                headers.put("username",usernameEditText.getText().toString());
                headers.put("password",passwordEditText.getText().toString());
                return headers;
            }
        };

        RequestQueue requestQueue=Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }
}
