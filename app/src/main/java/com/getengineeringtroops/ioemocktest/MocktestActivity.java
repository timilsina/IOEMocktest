package com.getengineeringtroops.ioemocktest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SlidingDrawer;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

public class MocktestActivity extends AppCompatActivity {

    int countdown=0;
    public int count=0;
    int questionCount;
    JSONObject jsonObject=null;
    int[] selected;
    int score=0;
    public static final String JSON_ARRAY="questions";
    public static final String QUESTION="question";
    public static final String OPTION_1="optiona";
    public static final String OPTION_2="optionb";
    public static final String OPTION_3="optionc";
    public static final String OPTION_4="optiond";
    public static final String ANSWER="answer";
    JSONArray questions=null;
    String[] question;
    String[] optionA;
    String[] optionB;
    String[] optionC;
    String[] optionD;
    String[] answer;
    TextView textViewQuestion;

    //views
    RadioGroup radioGroup;
    RadioButton radioButtonOptionA;
    WebView webViewA;
    RadioButton radioButtonOptionB;
    RadioButton radioButtonOptionC;
    RadioButton radioButtonOptionD;
    Button buttonNext;
    Button buttonPrevious;
    TextView textViewQuestionNumber;
    ProgressBar progressBar;

    GridView gridView;
    GridViewAdapter gridViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mocktest);

        selected=new int[100];

        //chronometer start and timer
        Chronometer chronometer=(Chronometer) findViewById(R.id.chronometer);
        chronometer.start();
        chronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                countdown++;
                if (countdown == 300) {
                    Intent intent = new Intent(MocktestActivity.this, ResultActivity.class);
                    startActivity(intent);
                }
            }
        });

        //related views
        textViewQuestion=(TextView) findViewById(R.id.textViewQuestion);
        radioGroup=(RadioGroup) findViewById(R.id.myRadioGroup);
        radioButtonOptionA=(RadioButton) findViewById(R.id.radioButtonA);
        radioButtonOptionB=(RadioButton) findViewById(R.id.radioButtonB);
        radioButtonOptionC=(RadioButton) findViewById(R.id.radioButtonC);
        radioButtonOptionD=(RadioButton) findViewById(R.id.radioButtonD);
        buttonNext=(Button) findViewById(R.id.buttonNext);
        buttonPrevious=(Button) findViewById(R.id.buttonPrevious);
        textViewQuestionNumber=(TextView) findViewById(R.id.textViewQuestionNo);
        progressBar=(ProgressBar) findViewById(R.id.progressBarTest);
        gridView=(GridView) findViewById(R.id.gridView);

        //get Extra json object passed as string
        String json=getIntent().getStringExtra("json");
        try{
            jsonObject=new JSONObject(json);
            parseJson();
            setQuestions();
        }
        catch (Throwable t){
            Log.d("Mocktest", "Error in parsing json");
        }

//        webview test
        webViewA=new WebView(this);
        webViewA.getSettings().setJavaScriptEnabled(true);
        webViewA.loadUrl("https://www.tuhh.de/MathJax/test/sample-tex.html");
        radioGroup.addView(webViewA,1);

        //gridview
        gridViewAdapter=new GridViewAdapter(this, questionCount);
        gridView.setAdapter(gridViewAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                count=position;
                setQuestions();
                SlidingDrawer slidingDrawer=(SlidingDrawer) findViewById(R.id.slidingDrawer);
                slidingDrawer.animateClose();
            }
        });
    }

    public void parseJson(){
        try {
            questions = jsonObject.getJSONArray(JSON_ARRAY);
            questionCount=questions.length();
            question=new String[questions.length()];
            optionA=new String[questions.length()];
            optionB=new String[questions.length()];
            optionC=new String[questions.length()];
            optionD=new String[questions.length()];
            answer=new String[questions.length()];
            for(int i=0;i<questions.length();i++){
                JSONObject object=questions.getJSONObject(i);
                question[i]=object.getString(QUESTION);
                optionA[i]=object.getString(OPTION_1);
                optionB[i]=object.getString(OPTION_2);
                optionC[i]=object.getString(OPTION_3);
                optionD[i]=object.getString(OPTION_4);
                answer[i]=object.getString(ANSWER);
            }
        }
        catch (JSONException e){
            e.printStackTrace();
        }
    }

    public void setQuestions(){
        textViewQuestion.setText(question[count]);
        radioButtonOptionA.setText(optionA[count]);
        radioButtonOptionB.setText(optionB[count]);
        radioButtonOptionC.setText(optionC[count]);
        radioButtonOptionD.setText(optionD[count]);
        setProgressBar();
        setQuestionNumber();
        try{
            radioGroup.check(selected[count]);
            Log.d("Selected log","count:"+count);
        }
        catch (Exception e){
            radioGroup.clearCheck();
            e.printStackTrace();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        countdown=0;
    }

    public void nextClicked(View v){
        saveCheckedState();
        if(count<(questionCount-1)){
            count++;
            setQuestions();
            buttonPrevious.setVisibility(View.VISIBLE);
        }
        else{
            buttonNext.setVisibility(View.GONE);
        }
    }

    public void previousClicked(View v){
        saveCheckedState();
        if(count>=1){
            count--;
            buttonNext.setVisibility(View.VISIBLE);
            setQuestions();
            if(count==0){
                buttonPrevious.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle state){
        state.putInt("count",count);
        state.putIntArray("selected", selected);
    }
    @Override
    public void onRestoreInstanceState(Bundle state){
        if(state!=null) {
            count=state.getInt("count");
            selected = state.getIntArray("selected");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void saveCheckedState(){
        try {
            int i = radioGroup.getCheckedRadioButtonId();
            selected[count]=i;
        }
        catch (Exception e){
            selected[count]=0;
            e.printStackTrace();
        }
    }
    public void calculateResult(){
        String[] userAnswer=getViewToCounter().clone();
        for(int i=0;i<answer.length;i++){
            if(userAnswer[i].equals(answer[i]))
                score++;
        }

    }
    public String[] getViewToCounter(){
        String[] a=new String[100];
        for(int i=0;i<100;i++){
            if(selected[i]==radioButtonOptionA.getId())
                a[i]="a";
            else if(selected[i]==radioButtonOptionB.getId())
                a[i]="b";
            else if(selected[i]==radioButtonOptionC.getId())
                a[i]="c";
            else if(selected[i]==radioButtonOptionD.getId())
                a[i]="d";
            else a[i]="z";
        }
        return a;
    }

    public void submitClicked(View v){
        calculateResult();
        Intent intent=new Intent(this,ResultActivity.class);
        intent.putExtra("result",score);
        startActivity(intent);
    }
    public void setProgressBar(){
        progressBar.setProgress(100*count/(questionCount-1));
    }

    public void setQuestionNumber(){
        textViewQuestionNumber.setText(count + 1 + "/" + questionCount);
    }
}
