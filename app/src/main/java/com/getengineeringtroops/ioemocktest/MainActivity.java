package com.getengineeringtroops.ioemocktest;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    String userName;
    String userEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Enter Coupon code", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                showRechargeDialog();
            }
        });

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //getting data passed through intent
//        userName=getIntent().getStringExtra("userName");
//        userEmail=getIntent().getStringExtra("userEmail");
        //String uriString=getIntent().getStringExtra("imageUri");
        //uri=Uri.parse(uriString);

        View header=navigationView.getHeaderView(0);

        //findView and display data
        TextView nameView=(TextView) header.findViewById(R.id.userName);
        TextView emailView=(TextView) header.findViewById(R.id.userEmail);
        nameView.setText(userName);
        emailView.setText(userEmail);

        if(getSupportFragmentManager().findFragmentById(R.id.frame_container)==null) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            IOEFragment ioeFragment = new IOEFragment();
            fragmentTransaction.add(R.id.frame_container, ioeFragment).commit();
        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.IOE) {
            FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction();
            IOEFragment fragment=new IOEFragment();
            fragmentTransaction.replace(R.id.frame_container,fragment).commit();

        } else if (id == R.id.IOM) {
            FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction();
            IOMFragment fragment=new IOMFragment();
            fragmentTransaction.replace(R.id.frame_container,fragment).commit();

        } else if (id == R.id.add_coupon) {
            showRechargeDialog();

        } else if (id == R.id.syllabus){
            FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction();
            SyllabusFragment fragment=new SyllabusFragment();
            fragmentTransaction.replace(R.id.frame_container,fragment).commit();

        } else if (id == R.id.faq){
            FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction();
            FAQFragment fragment=new FAQFragment();
            fragmentTransaction.replace(R.id.frame_container,fragment).commit();

        } else if (id == R.id.feedback){
            FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction();
            FeedbackFragment fragment=new FeedbackFragment();
            fragmentTransaction.replace(R.id.frame_container,fragment).commit();
        }
        else if (id == R.id.logOut){
            SharedPreferences sharedPreferences=getSharedPreferences("com.getengineeringtroops.ioemocktest", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor=sharedPreferences.edit();
            editor.remove("username");
            editor.remove("password");
            editor.commit();
            Intent intent=new Intent(this,LoginActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void showRechargeDialog(){
        DialogFragment dialog=new RechargeFragment();
        dialog.show(getSupportFragmentManager(), "RechargeFragment");
    }

    public void syllabusClicked(View view){
        FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction();
        SyllabusFragment fragment=new SyllabusFragment();
        fragmentTransaction.replace(R.id.frame_container,fragment,"SyllabusFragment").commit();
    }
}