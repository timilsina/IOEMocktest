package com.getengineeringtroops.ioemocktest;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SlidingDrawer;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

public class TestActivity extends AppCompatActivity {

    int countdown=0;
    public int count=0;
    int questionCount;
    int[] selected;
    int score=0;
    JSONArray questions=null;
    String[] question;
    String[] optionA;
    String[] optionB;
    String[] optionC;
    String[] optionD;
    String[] answer;
    TextView textViewQuestion;

    //views
    Button buttonNext;
    Button buttonPrevious;
    WebView webViewTest;
    TextView textViewQuestionNumber;
    ProgressBar progressBar;

    GridView gridView;
    GridViewAdapter gridViewAdapter;

    String url="file:///android_asset/mocktest.html";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        selected=new int[150];
        //get Extra json object passed as string
        String json=getIntent().getStringExtra("json");
        try{
            questions=new JSONArray(json);
            parseJson();
        }
        catch (Throwable t){
            Log.d("Mocktest", "Error in parsing json");
        }

        //chronometer start and timer
        Chronometer chronometer=(Chronometer) findViewById(R.id.chronometer);
        chronometer.start();
        chronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                countdown++;
                if (countdown == 300) {
                    Intent intent = new Intent(TestActivity.this, ResultActivity.class);
                    startActivity(intent);
                }
            }
        });

        //related views
        textViewQuestion=(TextView) findViewById(R.id.textViewQuestion);
        webViewTest=(WebView) findViewById(R.id.webViewTest);
        buttonNext=(Button) findViewById(R.id.buttonNext);
        buttonPrevious=(Button) findViewById(R.id.buttonPrevious);
        textViewQuestionNumber=(TextView) findViewById(R.id.textViewQuestionNo);
        progressBar=(ProgressBar) findViewById(R.id.progressBarTest);
        gridView=(GridView) findViewById(R.id.gridView);

        //JavaScriptInterface
        final TestJavaScriptInterface testJavaScriptInterface=new TestJavaScriptInterface(this);
        webViewTest.addJavascriptInterface(testJavaScriptInterface, "Android");
        webViewTest.getSettings().setJavaScriptEnabled(true);
        webViewTest.loadUrl(url);
        webViewTest.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                setQuestions();
            }
        });

        //gridview
        gridViewAdapter=new GridViewAdapter(this, questionCount);
        gridView.setAdapter(gridViewAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                count = position;
                setQuestions();
                SlidingDrawer slidingDrawer = (SlidingDrawer) findViewById(R.id.slidingDrawer);
                slidingDrawer.animateClose();
            }
        });
    }

    public void parseJson(){
        try {
            questionCount=questions.length();
            question=new String[questions.length()];
            optionA=new String[questions.length()];
            optionB=new String[questions.length()];
            optionC=new String[questions.length()];
            optionD=new String[questions.length()];
            answer=new String[questions.length()];
            for(int i=0;i<questions.length();i++){
                JSONObject object=questions.getJSONObject(i);
                JSONArray answers=object.getJSONArray("answer");
                question[i]=object.getString("question");
                optionA[i]=answers.getJSONObject(0).getString("answer");
                optionB[i]=answers.getJSONObject(1).getString("answer");
                optionC[i]=answers.getJSONObject(2).getString("answer");
                optionD[i]=answers.getJSONObject(3).getString("answer");
                answer[i]=object.getString("canswer");
            }
            Log.d("parseJson","json parsed"+question[0]);
        }
        catch (JSONException e){
            e.printStackTrace();
            Log.d("parseJson","error while parsing");
        }
    }

    public void setQuestions(){

        //webView test
        //webViewTest.loadUrl("javascript:setQuestion("+value1+","+value2+","+value3+","+value4+","+value5+","+")");

        try{
            Log.d("Selected log", "count:" + count);
            webViewTest.loadUrl("javascript:setQuestion('"+question[count]+"','"+optionA[count]+"','"+optionB[count]+"','"+optionC[count]+"','"+optionD[count]+"');");
            webViewTest.loadUrl("javascript:checkRadio("+selected[count]+");");
            setProgressBar();
            setQuestionNumber();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        countdown=0;
    }

    public void nextClicked(View v){
        saveCheckedState();
        if(count<(questionCount-1)){
            count++;
            setQuestions();
            buttonPrevious.setVisibility(View.VISIBLE);
        }
        else{
            buttonNext.setVisibility(View.GONE);
        }
    }

    public void previousClicked(View v){
        saveCheckedState();
        if(count>=1){
            count--;
            buttonNext.setVisibility(View.VISIBLE);
            setQuestions();
            if(count==0){
                buttonPrevious.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle state){
        state.putInt("count",count);
        state.putIntArray("selected", selected);
    }
    @Override
    public void onRestoreInstanceState(Bundle state){
        if(state!=null) {
            count=state.getInt("count");
            selected = state.getIntArray("selected");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void saveCheckedState(){
        try {
//            int i = radioGroup.getCheckedRadioButtonId();
//            selected[count]=i;
        }
        catch (Exception e){
            selected[count]=0;
            e.printStackTrace();
        }
    }
    public void calculateResult(){
        String[] userAnswer=getViewToCounter().clone();
        for(int i=0;i<answer.length;i++){
            if(userAnswer[i].equals(answer[i]))
                score++;
        }

    }
    public String[] getViewToCounter(){
        String[] a=new String[100];
        for(int i=0;i<100;i++){
//            if(selected[i]==radioButtonOptionA.getId())
//                a[i]="a";
//            else if(selected[i]==radioButtonOptionB.getId())
//                a[i]="b";
//            else if(selected[i]==radioButtonOptionC.getId())
//                a[i]="c";
//            else if(selected[i]==radioButtonOptionD.getId())
//                a[i]="d";
//            else a[i]="z";
        }
        return a;
    }

    public void submitClicked(View v){
        calculateResult();
        Intent intent=new Intent(this,ResultActivity.class);
        intent.putExtra("result",score);
        startActivity(intent);
    }
    public void setProgressBar(){
        progressBar.setProgress(100 * count / (questionCount - 1));
    }

    public void setQuestionNumber(){
        textViewQuestionNumber.setText(count + 1 + "/" + questionCount);
    }
    public class TestJavaScriptInterface{
        Context context;
        TestJavaScriptInterface(Context mContext){
            this.context=mContext;
        }

        @JavascriptInterface
        public void option1Clicked(){
            selected[count]=1;
        }

        @JavascriptInterface
        public void option2Clicked(){
            selected[count]=2;
        }

        @JavascriptInterface
        public void option3Clicked(){
            selected[count]=3;
        }

        @JavascriptInterface
        public void option4Clicked(){
            selected[count]=4;
        }

        @JavascriptInterface
        public void optionClicked(int value){
            selected[count]=value;
        }

    }
}
