package com.getengineeringtroops.ioemocktest;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;

/**
 *Created by phantom on 1/24/16.
 */
public class SyllabusFragment extends Fragment{

    public TabLayout tabLayout;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_syllabus,container,false);
    }

    @Override
    public void onStart() {
        super.onStart();
        tabLayout=(TabLayout) getActivity().findViewById(R.id.tabLayoutSyllabus);
        tabLayout.addTab(tabLayout.newTab().setText("IOE"));
        tabLayout.addTab(tabLayout.newTab().setText("IOM"));

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            TableLayout tableLayoutIOE = (TableLayout) getActivity().findViewById(R.id.tableLayoutIOE);
            TableLayout tableLayoutIOM = (TableLayout) getActivity().findViewById(R.id.tableLayoutIOM);
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if(tab.getText()=="IOE") {
                    tableLayoutIOM.setVisibility(View.GONE);
                    tableLayoutIOE.setVisibility(View.VISIBLE);
                }
                if(tab.getText()=="IOM"){
                    tableLayoutIOE.setVisibility(View.GONE);
                    tableLayoutIOM.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
}
